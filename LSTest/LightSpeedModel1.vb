Imports System

Imports Mindscape.LightSpeed
Imports Mindscape.LightSpeed.Validation
Imports Mindscape.LightSpeed.Linq

Namespace LSTest

  <Serializable()> _
  <System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")> _
  <System.ComponentModel.DataObject> _
  Public Partial Class Keytable
    Inherits Entity(Of Integer)

    Private _nextId As Integer



    ''' <summary>Identifies the NextId entity attribute.</summary>
    Public Const NextIdField As String = "NextId"





    Public Sub New()
      MyBase.New(False)
      Initialize()
    End Sub
    
    Protected Sub New(initialize As Boolean)
      MyBase.New(initialize)
    End Sub
  
    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property NextId() As Integer
      Get
        Return [Get](_nextId, "NextId")
      End Get
      Set(ByVal value As Integer)
        [Set](_nextId, value, "NextId")
      End Set
    End Property





  End Class


  <Serializable()> _
  <System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")> _
  <System.ComponentModel.DataObject> _
  <Table("muxes")> _
  Public Partial Class Mux
    Inherits Entity(Of Integer)

    <ValidatePresence> _
    Private _name As String
    
    Private _type As Short
    
    <ValidatePresence> _
    Private _commanLine As String
    
    <ValidatePresence> _
    Private _outputFolder As String
    
    Private _position As Short
    
    <Column("MuxList_Id")> _
    Private _muxListId As Integer



    ''' <summary>Identifies the Name entity attribute.</summary>
    Public Const NameField As String = "Name"
    ''' <summary>Identifies the Type entity attribute.</summary>
    Public Const TypeField As String = "Type"
    ''' <summary>Identifies the CommanLine entity attribute.</summary>
    Public Const CommanLineField As String = "CommanLine"
    ''' <summary>Identifies the OutputFolder entity attribute.</summary>
    Public Const OutputFolderField As String = "OutputFolder"
    ''' <summary>Identifies the Position entity attribute.</summary>
    Public Const PositionField As String = "Position"
    ''' <summary>Identifies the MuxListId entity attribute.</summary>
    Public Const MuxListIdField As String = "MuxListId"



    <ReverseAssociation("Muxes")> _
    Private ReadOnly _muxList As New EntityHolder(Of Muxlist)()
    <ReverseAssociation("Mux")> _
    Private ReadOnly _muxuploads As New EntityCollection(Of muxuploads)()

    Private _uploads As ThroughAssociation(Of muxuploads, Upload)

    Public Sub New()
      MyBase.New(False)
      Initialize()
    End Sub
    
    Protected Sub New(initialize As Boolean)
      MyBase.New(initialize)
    End Sub
  
    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property Name() As String
      Get
        Return [Get](_name, "Name")
      End Get
      Set(ByVal value As String)
        [Set](_name, value, "Name")
      End Set
    End Property

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property Type() As Short
      Get
        Return [Get](_type, "Type")
      End Get
      Set(ByVal value As Short)
        [Set](_type, value, "Type")
      End Set
    End Property

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property CommanLine() As String
      Get
        Return [Get](_commanLine, "CommanLine")
      End Get
      Set(ByVal value As String)
        [Set](_commanLine, value, "CommanLine")
      End Set
    End Property

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property OutputFolder() As String
      Get
        Return [Get](_outputFolder, "OutputFolder")
      End Get
      Set(ByVal value As String)
        [Set](_outputFolder, value, "OutputFolder")
      End Set
    End Property

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property Position() As Short
      Get
        Return [Get](_position, "Position")
      End Get
      Set(ByVal value As Short)
        [Set](_position, value, "Position")
      End Set
    End Property

    ''' <summary>Gets or sets the ID for the <see cref="MuxList" /> property.</summary>
    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property MuxListId() As Integer
      Get
        Return [Get](_muxListId, "MuxListId")
      End Get
      Set(ByVal value As Integer)
        [Set](_muxListId, value, "MuxListId")
      End Set
    End Property



    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property MuxList() As Muxlist
      Get
        Return [Get](Of Muxlist)(_muxList)
      End Get
      Set
        [Set](Of Muxlist)(_muxList, value)
      End Set
    End Property

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public ReadOnly Property muxuploads As EntityCollection(Of muxuploads)
      Get
        Return [Get](Of muxuploads)(_muxuploads)
      End Get
    End Property


    <System.Diagnostics.DebuggerNonUserCode()> _
    Public ReadOnly Property Uploads() As ThroughAssociation(Of muxuploads, Upload)
      Get
        If _uploads Is Nothing Then
          _uploads = New ThroughAssociation(Of muxuploads, Upload)(_muxuploads)
        End If
        Return [Get](Of ThroughAssociation(Of muxuploads, Upload))(_uploads)
      End Get
    End Property
    

  End Class


  <Serializable()> _
  <System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")> _
  <System.ComponentModel.DataObject> _
  <Table("muxlists")> _
  Public Partial Class Muxlist
    Inherits Entity(Of Integer)

    <ValidatePresence, ValidateLength(0, 25)> _
    Private _name As String



    ''' <summary>Identifies the Name entity attribute.</summary>
    Public Const NameField As String = "Name"



    <ReverseAssociation("MuxList")> _
    Private ReadOnly _muxes As New EntityCollection(Of Mux)()


    Public Sub New()
      MyBase.New(False)
      Initialize()
    End Sub
    
    Protected Sub New(initialize As Boolean)
      MyBase.New(initialize)
    End Sub
  
    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property Name() As String
      Get
        Return [Get](_name, "Name")
      End Get
      Set(ByVal value As String)
        [Set](_name, value, "Name")
      End Set
    End Property



    <System.Diagnostics.DebuggerNonUserCode()> _
    Public ReadOnly Property Muxes As EntityCollection(Of Mux)
      Get
        Return [Get](Of Mux)(_muxes)
      End Get
    End Property



  End Class


  <Serializable()> _
  <System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")> _
  <System.ComponentModel.DataObject> _
  <Table("uploads")> _
  Public Partial Class Upload
    Inherits Entity(Of Integer)

    <ValidateLength(0, 100)> _
    Private _username As String
    
    <ValidateLength(0, 256)> _
    Private _password As String
    
    Private _host As String
    
    Private _path As String
    
    Private _port As System.Nullable(Of Short)
    
    Private _type As System.Nullable(Of Short)
    
    <ValidateLength(0, 25)> _
    Private _name As String
    
    <ValidatePresence> _
    Private _domain As String



    ''' <summary>Identifies the Username entity attribute.</summary>
    Public Const UsernameField As String = "Username"
    ''' <summary>Identifies the Password entity attribute.</summary>
    Public Const PasswordField As String = "Password"
    ''' <summary>Identifies the Host entity attribute.</summary>
    Public Const HostField As String = "Host"
    ''' <summary>Identifies the Path entity attribute.</summary>
    Public Const PathField As String = "Path"
    ''' <summary>Identifies the Port entity attribute.</summary>
    Public Const PortField As String = "Port"
    ''' <summary>Identifies the Type entity attribute.</summary>
    Public Const TypeField As String = "Type"
    ''' <summary>Identifies the Name entity attribute.</summary>
    Public Const NameField As String = "Name"
    ''' <summary>Identifies the Domain entity attribute.</summary>
    Public Const DomainField As String = "Domain"



    <ReverseAssociation("Upload")> _
    Private ReadOnly _muxuploads As New EntityCollection(Of muxuploads)()

    Private _muxes As ThroughAssociation(Of muxuploads, Mux)

    Public Sub New()
      MyBase.New(False)
      Initialize()
    End Sub
    
    Protected Sub New(initialize As Boolean)
      MyBase.New(initialize)
    End Sub
  
    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property Username() As String
      Get
        Return [Get](_username, "Username")
      End Get
      Set(ByVal value As String)
        [Set](_username, value, "Username")
      End Set
    End Property

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property Password() As String
      Get
        Return [Get](_password, "Password")
      End Get
      Set(ByVal value As String)
        [Set](_password, value, "Password")
      End Set
    End Property

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property Host() As String
      Get
        Return [Get](_host, "Host")
      End Get
      Set(ByVal value As String)
        [Set](_host, value, "Host")
      End Set
    End Property

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property Path() As String
      Get
        Return [Get](_path, "Path")
      End Get
      Set(ByVal value As String)
        [Set](_path, value, "Path")
      End Set
    End Property

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property Port() As System.Nullable(Of Short)
      Get
        Return [Get](_port, "Port")
      End Get
      Set(ByVal value As System.Nullable(Of Short))
        [Set](_port, value, "Port")
      End Set
    End Property

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property Type() As System.Nullable(Of Short)
      Get
        Return [Get](_type, "Type")
      End Get
      Set(ByVal value As System.Nullable(Of Short))
        [Set](_type, value, "Type")
      End Set
    End Property

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property Name() As String
      Get
        Return [Get](_name, "Name")
      End Get
      Set(ByVal value As String)
        [Set](_name, value, "Name")
      End Set
    End Property

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property Domain() As String
      Get
        Return [Get](_domain, "Domain")
      End Get
      Set(ByVal value As String)
        [Set](_domain, value, "Domain")
      End Set
    End Property



    <System.Diagnostics.DebuggerNonUserCode()> _
    Public ReadOnly Property muxuploads As EntityCollection(Of muxuploads)
      Get
        Return [Get](Of muxuploads)(_muxuploads)
      End Get
    End Property


    <System.Diagnostics.DebuggerNonUserCode()> _
    Public ReadOnly Property Muxes() As ThroughAssociation(Of muxuploads, Mux)
      Get
        If _muxes Is Nothing Then
          _muxes = New ThroughAssociation(Of muxuploads, Mux)(_muxuploads)
        End If
        Return [Get](Of ThroughAssociation(Of muxuploads, Mux))(_muxes)
      End Get
    End Property
    

  End Class


  <Serializable()> _
  <System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")> _
  <System.ComponentModel.DataObject> _
  <Table("settings")> _
  Public Partial Class Setting
    Inherits Entity(Of Integer)

    <ValidateLength(0, 45)> _
    Private _projectName As String



    ''' <summary>Identifies the ProjectName entity attribute.</summary>
    Public Const ProjectNameField As String = "ProjectName"





    Public Sub New()
      MyBase.New(False)
      Initialize()
    End Sub
    
    Protected Sub New(initialize As Boolean)
      MyBase.New(initialize)
    End Sub
  
    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property ProjectName() As String
      Get
        Return [Get](_projectName, "ProjectName")
      End Get
      Set(ByVal value As String)
        [Set](_projectName, value, "ProjectName")
      End Set
    End Property





  End Class


  <Serializable()> _
  <System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")> _
  <System.ComponentModel.DataObject> _
  Public Partial Class muxuploads
    Inherits Entity(Of Integer)

    Private _muxId As Integer
    
    Private _uploadId As Integer



    ''' <summary>Identifies the MuxId entity attribute.</summary>
    Public Const MuxIdField As String = "MuxId"
    ''' <summary>Identifies the UploadId entity attribute.</summary>
    Public Const UploadIdField As String = "UploadId"



    <ReverseAssociation("muxuploads")> _
    Private ReadOnly _mux As New EntityHolder(Of Mux)()
    <ReverseAssociation("muxuploads")> _
    Private ReadOnly _upload As New EntityHolder(Of Upload)()


    Public Sub New()
      MyBase.New(False)
      Initialize()
    End Sub
    
    Protected Sub New(initialize As Boolean)
      MyBase.New(initialize)
    End Sub
  
    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property MuxId() As Integer
      Get
        Return [Get](_muxId, "MuxId")
      End Get
      Set(ByVal value As Integer)
        [Set](_muxId, value, "MuxId")
      End Set
    End Property

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property UploadId() As Integer
      Get
        Return [Get](_uploadId, "UploadId")
      End Get
      Set(ByVal value As Integer)
        [Set](_uploadId, value, "UploadId")
      End Set
    End Property



    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property Mux() As Mux
      Get
        Return [Get](Of Mux)(_mux)
      End Get
      Set
        [Set](Of Mux)(_mux, value)
      End Set
    End Property

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Property Upload() As Upload
      Get
        Return [Get](Of Upload)(_upload)
      End Get
      Set
        [Set](Of Upload)(_upload, value)
      End Set
    End Property



  End Class




  ''' <summary>
  ''' Provides a strong-typed unit of work for working with the dc model.
  ''' </summary>
  <System.CodeDom.Compiler.GeneratedCode("LightSpeedModelGenerator", "1.0.0.0")> _
  Public Partial Class dcUnitOfWork
    Inherits Mindscape.LightSpeed.UnitOfWork
    

    Public ReadOnly Property Keytables As System.Linq.IQueryable(Of Keytable)
      Get
        Return Query(Of Keytable)()
      End Get
    End Property
    
    Public ReadOnly Property Muxes As System.Linq.IQueryable(Of Mux)
      Get
        Return Query(Of Mux)()
      End Get
    End Property
    
    Public ReadOnly Property Muxlists As System.Linq.IQueryable(Of Muxlist)
      Get
        Return Query(Of Muxlist)()
      End Get
    End Property
    
    Public ReadOnly Property Uploads As System.Linq.IQueryable(Of Upload)
      Get
        Return Query(Of Upload)()
      End Get
    End Property
    
    Public ReadOnly Property Settings As System.Linq.IQueryable(Of Setting)
      Get
        Return Query(Of Setting)()
      End Get
    End Property
    
    Public ReadOnly Property muxuploads As System.Linq.IQueryable(Of muxuploads)
      Get
        Return Query(Of muxuploads)()
      End Get
    End Property
    
    
  End Class
  

End Namespace
