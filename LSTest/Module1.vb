﻿Imports Mindscape.LightSpeed
Imports LSTest.LSTest

Module Main

    Public LightSpeedContext As LightSpeedContext(Of dcUnitOfWork)



    Sub Main()


        LightSpeedContext = New LightSpeedContext(Of dcUnitOfWork)("Development")
        Dim dc As dcUnitOfWork = LightSpeedContext.CreateUnitOfWork()



        'Not Working
        For Each Mux In dc.Muxes
            Console.WriteLine("Mux: " + Mux.Name)

            For Each Upload In Mux.Uploads
                Console.WriteLine("Upload: " + Upload.Name)
            Next

        Next


        Console.WriteLine("----------------------------------------------------")


        'Working
        For Each Mux In dc.Muxes
            Console.WriteLine("Mux: " + Mux.Name)
            For Each mu As muxuploads In Mux.muxuploads.Where(Function(muxupload) muxupload.MuxId = Mux.Id)
                Dim up As Upload = dc.Uploads.Single(Function(upload) upload.Id = mu.UploadId)
                Console.WriteLine("Upload: " + up.Name)
            Next

        Next




        dc.Dispose()
    End Sub

End Module
